<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-internationalizable-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusBoth;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Internationalizable\InternationalizableStatusYesOnly;
use PHPUnit\Framework\TestCase;

/**
 * InternationalizableStatusYesOnlyTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Internationalizable\InternationalizableStatusYesOnly
 *
 * @internal
 *
 * @small
 */
class InternationalizableStatusYesOnlyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InternationalizableStatusYesOnly
	 */
	protected InternationalizableStatusYesOnly $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testIsAllowedNonInternationalizable() : void
	{
		$this->assertFalse($this->_object->isAllowedNonInternationalizable());
	}
	
	public function testIsAllowedInternationalizable() : void
	{
		$this->assertTrue($this->_object->isAllowedInternationalizable());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testMergeWithSelf() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith($this->_object));
	}
	
	public function testMergeWithOther() : void
	{
		$this->assertEquals(new InternationalizableStatusBoth(), $this->_object->mergeWith(new InternationalizableStatusNoOnly()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InternationalizableStatusYesOnly();
	}
	
}
