<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-internationalizable-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Internationalizable;

/**
 * InternationalizableStatusNoOnly class file.
 * 
 * This class represents an internationalizable status with no
 * internationalization.
 * 
 * @author Anastaszor
 */
class InternationalizableStatusNoOnly implements InternationalizableStatusInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Internationalizable\InternationalizableStatusInterface::isAllowedNonInternationalizable()
	 */
	public function isAllowedNonInternationalizable() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Internationalizable\InternationalizableStatusInterface::isAllowedInternationalizable()
	 */
	public function isAllowedInternationalizable() : bool
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Internationalizable\InternationalizableStatusInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Internationalizable\InternationalizableStatusInterface::mergeWith()
	 */
	public function mergeWith(InternationalizableStatusInterface $other) : InternationalizableStatusInterface
	{
		if($other->isAllowedInternationalizable())
		{
			return new InternationalizableStatusBoth();
		}
		
		return $this;
	}
	
}
